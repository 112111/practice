import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: 'Dashboard', icon: 'dashboard' }
    }]
  },
  {
    path: '/Таблицы',
    component: Layout,
    name: 'Tables',
    meta: { title: 'Таблицы', icon: 'table' },
    children: [
      {
        path: 'Компании',
        name: 'Table-companies',
        component: () => import('@/views/tables/table-companies/index'),
        meta: { title: 'Компании', icon: 'table' }
      },
      {
        path: 'Сотрудники',
        name: 'Table-employees',
        component: () => import('@/views/tables/table-employees/index'),
        meta: { title: 'Сотрудники', icon: 'table' }
      },
      {
        path: 'Проекты',
        name: 'Table-projects',
        component: () => import('@/views/tables/table-projects/index'),
        meta: { title: 'Проекты', icon: 'table' }
      },
      {
      path: 'Документы',
      name: 'Table-documents',
      component: () => import('@/views/tables/table-documents/index'),
      meta: { title: 'Документы', icon: 'table' }
      },
      {
        path: 'Конкурсы',
        name: 'Table-contests',
        component: () => import('@/views/tables/table-contests/index'),
        meta: { title: 'Конкурсы', icon: 'table' }
      },
      {
        path: 'Новости',
        name: 'Table-news',
        component: () => import('@/views/tables/table-news/index'),
        meta: { title: 'Новости', icon: 'table' }
      },
      {
        path: 'Реестры',
        name: 'Table-registers',
        component: () => import('@/views/tables/table-registries/index'),
        meta: { title: 'Реестры', icon: 'table' }
      }
    ]
  },

  {
    path: '/Добавить',
    component: Layout,
    name: 'forms',
    meta: { title: 'Добавить', icon: 'form' },
    children: [
      {
        path: 'Компанию',
        name: 'Form-company',
        component: () => import('@/views/form/form-company/index'),
        meta: { title: 'Компанию', icon: 'form' }
      },
      {
        path: 'Сотрудника',
        name: 'Form-employee',
        component: () => import('@/views/form/form-employee/index'),
        meta: { title: 'Сотрудника', icon: 'form' }
      },
      {
        path: 'Проект',
        name: 'Form-project',
        component: () => import('@/views/form/form-project/index'),
        meta: { title: 'Проект', icon: 'form' }
      },
      {
        path: 'Документ',
        name: 'Form-document',
        component: () => import('@/views/form/form-document/index'),
        meta: { title: 'Документ', icon: 'form' }
      },
      {
        path: 'Конкурс',
        name: 'Form-contests',
        component: () => import('@/views/form/form-contest/index'),
        meta: { title: 'Конкурс', icon: 'form' }
      },
      {
        path: 'Новости',
        name: 'Form-news',
        component: () => import('@/views/form/form-news/index'),
        meta: { title: 'Новость', icon: 'form' }
      },
      {
        path: 'Реестр',
        name: 'Form-register',
        component: () => import('@/views/form/form-registry/index'),
        meta: { title: 'Реестр', icon: 'form' }
      }
    ]
  },


  {
    path: 'external-link',
    component: Layout,
    children: [
      {
        path: 'https://farid-stkha.wixsite.com/oidtt',
        meta: { title: 'External Link', icon: 'link' }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
